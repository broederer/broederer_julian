#pragma once
#include <iostream> 
#include <string>
#include <sstream>
#include <mutex>
#include <thread>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

class Output{
    static std::shared_ptr<spdlog::logger> verboseLog;   
    static std::shared_ptr<spdlog::logger> debugLog;
    static std::shared_ptr<spdlog::logger> errorLog;
    Output(){};
    public:

        static void setLvlDebug();

        static void setLvlVerbose();

        template<typename... Args>
        static void printVerbose(spdlog::string_view_t str, const Args &... args) {
            verboseLog->info(str, args...);
        }

        template<typename... Args>
        static void printDebug(spdlog::string_view_t str, const Args &... args) {
            debugLog->debug(str, args...);
        }

        template<typename... Args>
        static void printError(spdlog::string_view_t str, const Args &... args) {
            errorLog->error(str, args...);
        }

        static void print(std::string stream);

};