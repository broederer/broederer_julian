#pragma once
#include <iostream>
#include <mutex>
#include <condition_variable>

class Semaphore{
    int count{0};
    std::mutex mtx;
    std::condition_variable cv;

    public:
        Semaphore(){};
        Semaphore(int count):count{count}{};

        void acquire() {
            std::unique_lock<std::mutex> lck{mtx};
            cv.wait(lck, [this]{ return count; });
            --count;
        }

        void release() {
            std::lock_guard<std::mutex> lck{mtx};
            ++count;
            cv.notify_one();
        }

        int getStatus(){
            std::lock_guard<std::mutex> lck{mtx};
            return this->count;
        }
};