#pragma once
#include <mutex>
#include <thread>
#include <chrono>
#include "semaphore.h"
#include "output.h"

class Barber{
    int n; //max anzahl custumer im shop
    int customers{0};
    
    public:
        Semaphore mtx{1};
        Semaphore customer{};
        Semaphore barber{};
        Semaphore customerDone{};
        Semaphore barberDone{};

        Barber(int n):n{n}{};

        void operator()(){
            Output::print("B: Barber is ready for work");
            while(true){
                Output::print("B: Barber checks for customer" );
                this->customer.acquire();
                Output::print("B: Barber found customer" );
                this->barber.release();

                Output::print("B: Barber is cutting hair ..." );
                std::this_thread::sleep_for(std::chrono::seconds(2));

                this->customerDone.acquire();

                this->barberDone.release();
                Output::print("B: Barber finished cutting hair" );
            }
        }

        int getMaxCust() {
            return this->n;
        }

        int getCurrCust() {
            return this->customers;
        }

        void increaseCurrCust() {
            this->customers++;
        }

        void decreaseCurrCust() {
            this->customers--;
        }

};