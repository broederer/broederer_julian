
#include "output.h"

using namespace std;

std::shared_ptr<spdlog::logger> Output::verboseLog = spdlog::stdout_color_mt("verbose");    
std::shared_ptr<spdlog::logger> Output::debugLog = spdlog::stdout_color_mt("debug");   
std::shared_ptr<spdlog::logger> Output::errorLog = spdlog::stderr_color_mt("error");

void Output::setLvlDebug() {
    debugLog->set_level(spdlog::level::debug); 
}

void Output::setLvlVerbose() {
    verboseLog->set_level(spdlog::level::critical); 
}

void Output::print(string stream) {
    static mutex mtx;
    static stringstream ss;
    lock_guard<mutex> lock(mtx);
    ss.str("");
    ss << stream << endl;
    cout << ss.str(); 
}

