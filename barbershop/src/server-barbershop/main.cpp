#include <iostream> 
#include <sstream>
#include <string>
#include <queue>
#include <chrono>
#include <thread>
#include <mutex>
#include <asio.hpp>
#include "CLI11.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "semaphore.h"
#include "barber.h"
#include "output.h"

using namespace std;
using namespace asio::ip;


void enterBarberShop(tcp::iostream strm, Barber& barb, int thId){
    bool full = false;
    
    auto myid = this_thread::get_id();
    stringstream ss;
    ss << myid;
    string threadId = ss.str();
    
    
    Output::printDebug("CT{} ThreadId: {}", thId, threadId);

    Output::printDebug("CT{}: mtx status count: {}", thId, barb.mtx.getStatus());
    barb.mtx.acquire();
        
    Output::printDebug("CT{}: if max customer: {} > count customer: {}",thId ,barb.getMaxCust() ,barb.getCurrCust());
    if(barb.getMaxCust() > barb.getCurrCust()){
        barb.increaseCurrCust();
    }else{
        strm << "Waitingroom is full - no seat is available" << endl;
        full = true;
    }
    Output::printVerbose("CT{}: release barb.mtx", thId);
    barb.mtx.release();


    if(!full){
        strm << "entering shop ..." << endl;

        Output::printVerbose("CT{}: customer.release()", thId);
        barb.customer.release();
        Output::printVerbose("CT{}: barber.acquire()", thId);
        barb.barber.acquire();

        strm << "getting hair cut .." << endl;

        Output::printVerbose("CT{}: customerDone.release()", thId);
        barb.customerDone.release();

        strm << "waiting for barbar finish cutting ... " << endl;
        Output::printVerbose("CT{}: barberDone.acquire()", thId);
        barb.barberDone.acquire();

        Output::printVerbose("CT{}: locking mtx for curr customer count", thId);
        barb.mtx.acquire();
        Output::printVerbose("CT{}: decrease(1) curr customer count ", thId);
        barb.decreaseCurrCust();
        strm << "finished" << endl;
        Output::printVerbose("CT{}: release mtx for curr customer count", thId);
        barb.mtx.release();        
    }
    
    Output::printVerbose("CT{}: close stream to client", thId);
    strm << endl;
    strm.close();
}

int main(int argc, char* argv[]) {
    CLI::App app{"", "barber"};
    unsigned short port;
    int space;
    app.add_option("-p", port, "port")->default_val("9999");
    app.add_option("-s", space, "space")->default_val(3);
    auto verbose = app.add_flag("-v, --verbose", "for more info");
    auto debug = app.add_flag("-d, --debug", "for debugging output");
    CLI11_PARSE(app, argc, argv);

    if(*debug) {
        Output::setLvlDebug();
    }

    if(!*verbose) {
        Output::setLvlVerbose();
    }
    
    Output::printVerbose("opening barbershop with {} waiting seats available!", space);
    
    asio::io_context ctx;

    tcp::endpoint ep{tcp::v4(), port};
    tcp::acceptor acceptor{ctx, ep};
    acceptor.listen();

    Barber barb{space};
    thread bt{ref(barb)};
    bt.detach();
    int thId{1};

    while (true) {
        Output::printVerbose("HT: waiting for stream");
        tcp::iostream strm{acceptor.accept()};
        Output::printVerbose("HT: stream found");

        Output::printVerbose("HT: starting new cust thread nr:{}", thId);
        thread t{enterBarberShop, move(strm), ref(barb), thId};
        Output::printVerbose("HT: detach new cust thread");
        t.detach();
        thId++;     
    }    
}