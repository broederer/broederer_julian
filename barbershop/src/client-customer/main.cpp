#include <iostream> 
#include <string>
#include <sstream>
#include <mutex>
#include <thread>

#include <asio.hpp>
#include "CLI11.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

#include "output.h"

using namespace std;
using namespace asio::ip;

int main(int argc, char* argv[]) { 
    CLI::App app{"", "customer"};
    unsigned short port;
    app.add_option("-p, --port", port, "port")->default_val(9999);
    auto verbose = app.add_flag("-v, --verbose", "for more info");
    auto debug = app.add_flag("-d, --debug", "for debugging output");
    CLI11_PARSE(app, argc, argv);

    if(*debug) {
        Output::setLvlDebug();
    }

    if(!*verbose) {
        Output::setLvlVerbose();
    }

    Output::printVerbose("connect to server");
    Output::printDebug("port:{}", port);
    tcp::iostream strm{"localhost", to_string(port)};
    if (strm) {
        Output::printVerbose("connecting success");
        string data;
        Output::printVerbose("waiting for stream from server(getline)");
        getline(strm, data);
        Output::print("customer wants to visit the barber");

        while (!data.empty()) {
            if (strm) {
                Output::printVerbose("stream valid");
                Output::print(data);
            }else{
                Output::printError("stream error: {}", strm.error().message());
            }
            Output::printVerbose("waiting for stream from server(getline)");
            getline(strm, data);
        }
        Output::printVerbose("close stream");
        strm.close();
    } else {
        Output::printError("error failed connecting to server: {}", strm.error().message());
    } 
}